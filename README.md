# calendar
一个基于jquery的日历板控件，界面设计比较随意，感兴趣可以使用，源码可随意修改。
![image](https://gitee.com/mluckydog/canlender/raw/master/img/calendar.png)

# 用法
$('').calendar(options);

# 参数
  defaultDate:'',//接收一个时间替代默认的当前时间   

  eventData:'',//接收事件参数,此参数存在时则url，params，method均不会生效  

  headerHeight:'60px',//表格头部高度  

  dialogMethod:'all',//查看事件详情的方式，默认为点击日期框查看全部（因当前项目弹框较复杂，暂未合并封装，故暂不支持弹框）  

  themes:'',//设置颜色主体（暂不支持）  

  params:{},//获取数据时需要上传的参数 

  url:'',//获取数据的地址，返回的数据格式为jsonArray  

  mehtod:'post',//获取数据的方式  

  注：若采用url方式，则每次请求时会自动带上year和month参数，如果后台做了处理，就只会查到当月的事件 

